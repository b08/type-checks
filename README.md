# @b08/type-checks, seeded from @b08/library-seed, library type: dry
Util methods isString, isObject and etc. are deprecated and it is recommended to use simpler checks.
For example:
```
/** @deprecated since v4.0.0 - use `typeof value === 'function'` instead. */
function isFunction(object: any): boolean;
```

This library encapsulates those checks, implementing recommended behavior.