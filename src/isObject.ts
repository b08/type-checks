export function isObject(toCheck: any): boolean {
  return toCheck != null && typeof toCheck === "object";
}
