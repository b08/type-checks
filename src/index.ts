export * from "./isFunction";
export * from "./isNumber";
export * from "./isObject";
export * from "./isString";
