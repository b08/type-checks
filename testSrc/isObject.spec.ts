import { test } from "@b08/test-runner";
import { isObject } from "../src";

test("isObject should return false for function", async t => {
  // arrange

  // act
  const result = isObject(() => null);

  // assert
  t.false(result);
});

test("isObject should return false for null", async t => {
  // arrange

  // act
  const result = isObject(null);

  // assert
  t.false(result);
});

test("isObject should return true for object", async t => {
  // arrange

  // act
  const result = isObject({});

  // assert
  t.true(result);
});

test("isObject should return true for null-prototype object", async t => {
  // arrange

  // act
  const result = isObject(Object.create(null));

  // assert
  t.true(result);
});

test("isObject should return true for class instance", async t => {
  // arrange

  // act
  const result = isObject(new Date());

  // assert
  t.true(result);
});
