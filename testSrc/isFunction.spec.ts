import { test } from "@b08/test-runner";
import { isFunction } from "../src";

test("isFunction should return true for function", async t => {
  // arrange
  function a(): void { return null; }
  // act
  const result = isFunction(a);

  // assert
  t.true(result);
});

test("isFunction should return true for object method", async t => {
  // arrange
  const a = {};

  // act
  const result = isFunction(a.hasOwnProperty);

  // assert
  t.true(result);
});

test("isFunction should return true for lambda", async t => {
  // arrange

  // act
  const result = isFunction(() => null);

  // assert
  t.true(result);
});

test("isFunction should return false for object", async t => {
  // arrange

  // act
  const result = isFunction({});

  // assert
  t.false(result);
});

test("isFunction should return false for null", async t => {
  // arrange

  // act
  const result = isFunction(null);

  // assert
  t.false(result);
});
