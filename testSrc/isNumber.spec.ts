import { test } from "@b08/test-runner";
import { isNumber } from "../src/isNumber";

test("isNumber should return true for number", async t => {
  // arrange
  const obj = 42;

  // act
  const result = isNumber(obj);

  // assert
  t.true(result);
});

test("isNumber should return false for string", async t => {
  // arrange
  const obj = "42";

  // act
  const result = isNumber(obj);

  // assert
  t.false(result);
});

test("isNumber should return false for object", async t => {
  // arrange
  const obj = {};

  // act
  const result = isNumber(obj);

  // assert
  t.false(result);
});
