import { test } from "@b08/test-runner";
import { isString } from "../src/isString";

test("isString should return true for string", async t => {
  // arrange
  const obj = "42";

  // act
  const result = isString(obj);

  // assert
  t.true(result);
});

test("isString should return false for number", async t => {
  // arrange
  const obj = 42;

  // act
  const result = isString(obj);

  // assert
  t.false(result);
});

test("isString should return false for object", async t => {
  // arrange
  const obj = {};

  // act
  const result = isString(obj);

  // assert
  t.false(result);
});
